package sibsutis.kchipson.laboratorywork1;

public class Math {
    /**
     * Функция, возвращающая наименьшее из чисел
     * @param a - (int) Первое число
     * @param b - (int) Второе число
     * @return - (int) Наименьшее из чисел
     */
    static int min(int a, int b){
        if (a < b)
            return a;

        else
            return b;
    }

    /**
     * Функция, возвращающая наибольшее из чисел
     * @param a - (int) Первое число
     * @param b - (int) Второе число
     * @return - (int) Наибольшее из чисел
     */
    static int max(int a, int b){
        if (a > b)
            return a;

        else
            return b;
    }
}
