package sibsutis.kchipson.laboratorywork1;

import static org.junit.Assert.*;

import org.junit.Test;

public class MainActivityTest {

    //#################################
    //   Tests for the "min" function
    //#################################

    @Test
    public void testMin() {
        int expected = 1;
        int actual = Math.min(1, 4);
        assertEquals(expected, actual);
    }

    @Test(expected = AssertionError.class)
    public void testMin_failed() {
        int expected = 4;
        int actual = Math.min(4, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testMin_extremeValues() {
        int expected = -2147483648;
        int actual = Math.min(-2147483648, 2147483647);
        assertEquals(expected, actual);
    }

    @Test
    public void testMin_equalValues() {
        int expected = 0;
        int actual = Math.min(0, 0);
        assertEquals(expected, actual);
    }

    //#################################
    //   Tests for the "max" function
    //#################################
    @Test
    public void testMax() {
        int expected = 4;
        int actual = Math.max(1, 4);
        assertEquals(expected, actual);
    }

    @Test(expected = AssertionError.class)
    public void testMax_failed() {
        int expected = 1;
        int actual = Math.max(4, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testMax_extremeValues() {
        int expected = 2147483647;
        int actual = Math.max(-2147483648, 2147483647);
        assertEquals(expected, actual);
    }

    @Test
    public void testMax_equalValues() {
        int expected = 0;
        int actual = Math.max(0, 0);
        assertEquals(expected, actual);
    }
}